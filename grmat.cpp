
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

// Znaczniki dla tablicy potem zmiana na wagi
#define UNVISITED 0
#define VISITED 1

#include "graph.h"

//macierzowa implementacja
class Graphm : public Graph {
private:
  int numVertex, numEdge; // liczba krawedzi i wierzcholkow
  int **matrix;           // wskaznik do macierzy sasiedztwa
  int *mark;              // wskaznik do tablicy wag
public:
  Graphm(int numVert) {   // konstruktor
    int i, j;
    numVertex = numVert;
    numEdge = 0;
    mark = new int[numVert];     // koluj pamiec na wagi
    for (i=0; i<numVertex; i++)
      mark[i] = UNVISITED;
    matrix = (int**) new int*[numVertex]; // i macierzy sasiedztwa
    for (i=0; i<numVertex; i++)
      matrix[i] = new int[numVertex];
    for (i=0; i< numVertex; i++) // zainicjuj  zerowymi wagami
      for (int j=0; j<numVertex; j++)
        matrix[i][j] = 0;
  }

  ~Graphm() {       // Destruktor
    delete [] mark; // oddaj pamiec
    for (int i=0; i<numVertex; i++)
      delete [] matrix[i];
    delete [] matrix;
  }

  int n() { return numVertex; } // liczba wierzcholkow
  int e() { return numEdge; }   // i krawedzi

  // dodaj krawedz
  void setEdge(int v1, int v2, int wgt) {
    if (matrix[v1][v2] == 0)
      numEdge++;                // zwieksz liczbe krawedzi
    matrix[v1][v2] = wgt;
  }

  void delEdge(int v1, int v2) { //usun krawedz (v1, v2)
    if (matrix[v1][v2] != 0) numEdge--;
    matrix[v1][v2] = 0;
  }

  int weight(int v1, int v2) { return matrix[v1][v2]; }
  int getMark(int v) { return mark[v]; }
  void setMark(int v, int val) { mark[v] = val; }

  // zwroc pierwszego sasiada
  int first(int v) {
    for (int i=0; i<numVertex; i++)
      if (matrix[v][i] != 0)
        return i;
    return numVertex;           // zwroc to jesli nie ma
  }

  // zwroc po v1 kolejnego sasiada po v2
  int next(int v1, int v2) {
    for(int i=v2+1; i<numVertex; i++)
      if (matrix[v1][i] != 0)
        return i;
    return numVertex;           // jak nie ma
  }

};

// tworzenei i wyswietlanie

#define LINELEN 80

void Gprint(Graph* G) {
  int i, j;

  cout << "Number of vertices is " << G->n() << "\n";
  cout << "Number of edges is " << G->e() << "\n";

  cout << "Macierz:\n";
  for (i=0; i<G->n(); i++) {
    for(j=0; j<G->n(); j++)
      cout << G->weight(i, j) << " ";
    cout << "\n";
  }
}


char* getl(char* buffer, int n, FILE* fid) {
  char* ptr;
  ptr = fgets(buffer, n, fid);
  while (ptr != NULL) 
    ptr = fgets(buffer, n, fid);
  return ptr;
}


// stworz z pliku fid
template <class GType>
Graph* createGraph(FILE* fid) {
  char buffer[LINELEN+1]; //bufor do czytanai lini
  int i;
  int v1, v2, dist;

  if (getl(buffer, LINELEN, fid) == NULL) 
{ cout << "nie mozna odczytac liczby wierzchlkow\n";
    return NULL;
}

  Graph* G = new GType(atoi(buffer));



  // czytaj krawedzie
  while (getl(buffer, LINELEN, fid) != NULL) {
    v1 = atoi(buffer);
    i = 0;
    
    while (isdigit(buffer[i])) i++; //sprawdza czy cyfra
    while (buffer[i] == ' ') i++;
    v2 = atoi(&buffer[i]);
    while (isdigit(buffer[i])) i++; //sprawdza czy cyfra
    if (buffer[i] == ' ') { // tu bedzie waga
      while (buffer[i] == ' ') i++;
      dist = atoi(&buffer[i]);
    }
    else dist = 1;
    G->setEdge(v1, v2, dist);
    if (1) 
      G->setEdge(v2, v1, dist);
  }
  return G;
}
