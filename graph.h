
class Graph {
public:

  virtual int n() =0;
  virtual int e() =0;
  virtual void setEdge(int v1, int v2, int wgt) =0;
  virtual void delEdge(int v1, int v2) =0;
  virtual int weight(int v1, int v2) =0;
  virtual int getMark(int v) =0;
  virtual void setMark(int v, int val) =0;
  virtual int first(int v) =0;
  virtual int next(int v1, int v2) =0;
};
