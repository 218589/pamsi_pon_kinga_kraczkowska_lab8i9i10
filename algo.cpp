#include <fstream>  
#include <stdio.h>
#include <iostream>
using namespace std;

#include "grmat.h"
#include "grlist.h"
#include "minheap.h"
#include "graph.h"
#include "Timer.hh"
#define INFINITY 9999

void AddEdgetoMST(int v1, int v2) {
  cout << "Dodano krawedz od " << v1 << " do" << v2 << "\n";
}


class PrimElem {
public:
  int vertex;
  int distance;
  PrimElem() { vertex = 0; distance = 0; }
  PrimElem(int v, int d) { vertex = v; distance = d; }
};


//Algorytm prime na kolejce priorytetowej bazujacej na kopcu
void Prim(Graph* G, int* D, int s) {
  int i, v, w;           // v to aktualny wierzcholek
  int V[G->n()];         // tab  o liczbie wierzcholkow
  PrimElem temp;
  PrimElem E[G->e()];    // o liczbie krawedzi
  temp.distance = 0; temp.vertex = s;
  E[0] = temp;           // tablica dla kopca
  minheap<PrimElem> H(E, 1, G->e()); // stworz kopiec (e wskaznik na pocz, liczba aktualnych elem, max roz )
  for (i=0; i<G->n(); i++) {           // buduj drzewo MST
    do {
      if(!H.removemin(temp)) return;   // jesli nic do usuniecia
      v = temp.vertex;
    } while (G->getMark(v) == VISITED);
    G->setMark(v, VISITED);
    if (v != s) AddEdgetoMST(V[v], v); // dodaj do drzewaMST
    if (D[v] == INFINITY) return;      // jak nie ma drogi do wierzcholka
    for (w=G->first(v); w<G->n(); w = G->next(v,w))
      if (D[w] > G->weight(v, w)) {    // zamien D
        D[w] = G->weight(v, w);
        V[w] = v;        // zmiana ppoczatku
        temp.distance = D[w]; temp.vertex = w;
        H.insert(temp);  // wstaw nowa wage do kolejki
      }
  }
}

// Dijkstry na kolejce na kopcu
void Dijkstra(Graph* G, int* D, int s) {
  int i, v, w;            // v aktualny wierzcholek
  PrimElem temp;
  PrimElem E[G->e()];     
  temp.distance = 0; temp.vertex = s;
  E[0] = temp;            
  minheap<PrimElem> H(E, 1, G->e()); 
  for (i=0; i<G->n(); i++) {         //dopoki sa wierzcholki
    do {
      if(!H.removemin(temp)) return; // nic do usuniecia
      v = temp.vertex;
    } while (G->getMark(v) == VISITED);
    G->setMark(v, VISITED);
    if (D[v] == INFINITY) return;    // nie ma przejscia do wierzcholka
    for (w=G->first(v); w<G->n(); w = G->next(v,w))
      if (D[w] > (D[v] + G->weight(v, w))) { // zmien D
        D[w] = D[v] + G->weight(v, w);
        temp.distance = D[w]; temp.vertex = w;
        H.insert(temp);   // dodaj nowa wage do kolejki
      }
  }
}


class KruskElem {         // elementy do kolejki
public:
  int from, to, distance; // info o krawedzi
  KruskElem() { from = -1;  to = -1; distance = -1; }
  KruskElem(int f, int t, int d)
    { from = f; to = t; distance = d; }
};


//na kolejce na kopcu
void Kruskel(Graph* G) {   // Kruskal MST 
  ParPtrTree A(G->n());    // klaseru konstruktor ktory 
  //robi tablice o rozmiarze
  KruskElem E[G->e()];     // tablica krawedzi dla kopca
  int i;
  int edgecnt = 0;
  for (i=0; i<G->n(); i++) // dodaj krawedz do tablicy
    for (int w=G->first(i); w<G->n(); w = G->next(i,w)) {
      E[edgecnt].distance = G->weight(i, w);
      E[edgecnt].from = i;
      E[edgecnt++].to = w;
    }
  // kolejka prorytetowa dla krawedzi
  minheap<KruskElem> H(E, edgecnt, edgecnt);
  int numMST = G->n();       //liczba klaserow
  for (i=0; numMST>1; i++) { 
    KruskElem temp;
    H.removemin(temp); // dodaj nastepna krawedz o najmniejszej wadze
    int v = temp.from;  int u = temp.to;
    if (A.differ(v, u)) {  // jesli rozne klasery
      A.UNION(v, u);       // to je sklej
      AddEdgetoMST(temp.from, temp.to);  // dodaj krawedz MST
      numMST--;            // jeden klaser mniej
    }
  }
}

main(int argc, char** argv) {
  Graph* G;
  FILE *fid;
  fstream plik;
  Timer tim1;
  plik.open("time.txt",std::fstream::app);

	if(!plik.is_open())
	{
	cout<<"nie mozna otworzyc pliku do zapisu"<<endl;
	return;
	}

  if (argc != 2) {
    cout << "podaj wywolanie\n";
    return;
  }

  if ((fid = fopen(argv[1], "r")) == NULL) {
    cout << "blad otawrcia pliku |" << argv[1] << "|\n";
    return;
  }

  G = createGraph<Graphm>(fid); //typu macierzowego
  if (G == NULL) {
    cout << "nie udalo sie stworzyc grafu\n";
    return;
  }
  /*****************************************************************************/
  int D[G->n()];//stworz tablice o liczbie krawedzi
  for (int i=0; i<G->n(); i++)     // inicjuj ustaw na nieskonczonosc
    D[i] = INFINITY;
  D[0] = 0;
  tim1.start();
  Prim(G, D, 0);
  tim1.stop();
   double Prim_T=tim1.getElapsedTimeInMilliSec();
   /***************************************************************************/
  /*for(int k=0; k<G->n(); k++) //wypisz wagi
    cout << D[k] << " ";
  cout << endl;*/
   tim1.start();
   Kruskel(G);
   tim1.stop();
   double Kruskel_T=tim1.getElapsedTimeInMilliSec();
   /******************************************************************************/ 
    int D1[G->n()];//stworz tablice o liczbie krawedzi
    for (int i=0; i<G->n(); i++)     // inicjuj ustaw na nieskonczonosc
    D1[i] = INFINITY;
    D1[0] = 0;
    tim1.start();
    Dijkstra(G,D1, 0);
    double Dijkstra_T=tim1.getElapsedTimeInMilliSec();
	//zapis wynikow pomiaru czasu do pliku
	plik<<"Prim "<<Prim_T<<endl<<"Krusk"<<Krusk_T<<endl<<"Dijk"<<Dijkstra_T<<endl;
	plik.close();
  
  
   return 0;
  
  
}
