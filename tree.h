//potrzebny do klasterow
class ParPtrTree {  
private:
  int* array;                    // tablica 
  int size;                      // rozmiar tablicy
  int FIND(int) const;           // znajdz korzen
public:
  ParPtrTree(int);               // konstruktor
  ~ParPtrTree() { delete [] array; } // destruktor
  void UNION(int, int);          // scalanie
  bool differ(int, int);         // porowanie
};

ParPtrTree::ParPtrTree(int sz) { // konstruktor
  size = sz;
  array = new int[sz];           // stworz tablice
  for(int i=0; i<sz; i++) array[i] = ROOT;
}

// zwraca prawde jesli wirzcholki roznych drzew
bool ParPtrTree::differ(int a, int b) {
  int root1 = FIND(a);           // znajdz korzen dla wierzcholka a
  int root2 = FIND(b);           // dla b
  return root1 != root2;         // porownaj krzenie
}

void ParPtrTree::UNION(int a, int b) { // scal poddrzewa
  int root1 = FIND(a);           // znajdz korzen
  int root2 = FIND(b);           // korzen
  if (root1 != root2) array[root2] = root1; // scal(zmien korzen)
}

int ParPtrTree::FIND(int curr) const {  
  if (array[curr] == ROOT) return curr; // znajdz korzen
    return array[curr] = FIND(array[curr]);
}
