

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "list.h"
#include "graph.h"

class Edge {
public:
  int vertex, weight;
  Edge() { vertex = -1; weight = -1; }
  Edge(int v, int w) { vertex = v; weight = w; }
};

ostream& operator << (ostream& s, Edge e)
  { return(s << "(" << e.vertex << ", " << e.weight << ")"); }


class Graphl : public Graph {
private:
  int numVertex, numEdge;     // liczby krawedzi i wierzcholkow
  List<Edge>** vertex; // lista wskaznikow na wierzcholki
  int *mark;                  // wskazniki na wagi
public:
  Graphl(int numVert) { // konstruktor grafu 
    int i, j;
    numVertex = numVert;
    numEdge = 0;
    mark = new int[numVert];  // stworzenie tablicy dla wag
    for (i=0; i<numVertex; i++) mark[i] = UNVISITED;
    // robi liste krawedzi
    vertex = (List<Edge>**) new List<Edge>*[numVertex];
    for (i=0; i<numVertex; i++)
      vertex[i] = new LList<Edge>();
  }

  ~Graphl() {       // destruktor
    delete [] mark; // zwraca pamiec  zalokowana na tablice
    for (int i=0; i<numVertex; i++) delete [] vertex[i];
    delete [] vertex;//i wierzcholki
  }

  int n() { return numVertex; } // zwrac liczbe wierzcholkow
  int e() { return numEdge; }   // i krawedzi

  // dodaj krawedz od v1 do v2 o wadze wgt
  void setEdge(int v1, int v2, int wgt) {
    Edge it(v2, wgt);
    Edge curr;
    vertex[v1]->getValue(curr);
   
    if (curr.vertex != v2)
      for (vertex[v1]->setStart();//funkcja dla listy
           vertex[v1]->getValue(curr); vertex[v1]->next())
        if (curr.vertex >= v2) break;
    if (curr.vertex == v2)   // jesli istnije
      vertex[v1]->remove(curr); // to usun
    else numEdge++;             // i wstaw nowa
    vertex[v1]->insert(it);//wstaw na poczatku listy
  }

  int getMark(int v) { return mark[v]; }//zwroc wage
  void setMark(int v, int val) { mark[v] = val; }//wstaw wage

  void delEdge(int v1, int v2) { // usun krawedz (v1, v2)
    Edge curr;
    vertex[v1]->getValue(curr);
    if (curr.vertex != v2)         //jesli nie v2 zostaje to znajdz v1
      for (vertex[v1]->setStart(); 
           vertex[v1]->getValue(curr); vertex[v1]->next())
        if (curr.vertex >= v2) break;
    // jesli v2 jest poczatkiem to usun
    if (curr.vertex == v2) {
      vertex[v1]->remove(curr);
      numEdge--;
    }
  }

  int weight(int v1, int v2) { // zwroc wage krawedzi (v1, v2)
    Edge curr;
    vertex[v1]->getValue(curr);
    if (curr.vertex != v2)         
      for (vertex[v1]->setStart(); 
           vertex[v1]->getValue(curr); vertex[v1]->next())
        if (curr.vertex >= v2) break;
    if (curr.vertex == v2)
      return curr.weight;
    else   // jesli nie nadano wagi
      return 0; // to waga to zero
  }

  int first(int v) { // zwroc pierwszego sasiada v
    Edge it;
    vertex[v]->setStart();
    if (vertex[v]->getValue(it)) return it.vertex;
    else return numVertex;      // zwraca jesli nie istniej
  }

  int next(int v1, int v2) { // zwaraca kolejnego sasiada po v1 i v2
    Edge it;
    vertex[v1]->getValue(it);
    if (it.vertex == v2)     // nastepny wierzcholek
      vertex[v1]->next();
    else { // jesli nie uda sie szukaj od poczatku listy
      vertex[v1]->setStart();
      while (vertex[v1]->getValue(it) && (it.vertex <= v2))
        vertex[v1]->next();
    }
    if (vertex[v1]->getValue(it)) return it.vertex;
    else return numVertex;      // jakby nie znalazl
  }
};

// wyswietlanie i wczytaywanie grafu  z pliku

#define LINELEN 80

void Gprint(Graph* G) {
  int i, j;

  cout << "Liczba wierzcholkow to " << G->n() << "\n";
  cout << "Liczba krawedzi to " << G->e() << "\n";

  cout << "Macierz:\n";
  for (i=0; i<G->n(); i++) {
    for(j=0; j<G->n(); j++)
      cout << G->weight(i, j) << " ";
    cout << "\n";
  }
}


char* getl(char* buffer, int n, FILE* fid) {
  char* ptr;
  ptr = fgets(buffer, n, fid);//czyta n-1 znakow jeli nie spotka konca lini/pliku 
  while (ptr != NULL) 
    ptr = fgets(buffer, n, fid);
  return ptr;
}


// stworz graf na podstawie pliku fid
template <class GType>
Graph* createGraph(FILE* fid) {
  char buffer[LINELEN+1]; // buffor do czytanai
  int i;
  int v1, v2, dist;

  if (getl(buffer, LINELEN, fid) == NULL) // nie mozna pobrac liczby wierzcholkow
{ cout << "nie mozna pobrac liczby wierzcholkow\n";
    return NULL;
}

  Graph* G = new GType(atoi(buffer));

  // czytaj krawedzie
  while (getl(buffer, LINELEN, fid) != NULL) {
    v1 = atoi(buffer);//konczy czytac jak spotka cos innego niz cyfta np spacje
    i = 0;
    while (isdigit(buffer[i])) i++;//czy cyfra
    while (buffer[i] == ' ') i++;
    v2 = atoi(&buffer[i]);
    while (isdigit(buffer[i])) i++; //sprawdz czy cyfra
    if (buffer[i] == ' ') { // to jest waga
      while (buffer[i] == ' ') i++;
      dist = atoi(&buffer[i]);
    }
    else dist = 1;
    G->setEdge(v1, v2, dist);
    if (1) 
      G->setEdge(v2, v1, dist);
  }
  return G;
}




